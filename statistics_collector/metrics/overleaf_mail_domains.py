import datetime
import sys
from typing import Optional

import pandas as pd
import requests

from .overleaf_api import OverleafApiMetric


class OverleafEmailDomains(OverleafApiMetric):
    """Gets distribution of email domains

    The API of overleaf-statistics is called, which returns the statistics in the needed way.

    Examples
    --------
    Example content of pandas DataFrame returned (splitted for length):
        datetime,hzdr.de,desy.de,...
        2022-05-11T10:03:40.432770+00:00,14,20,29,36
    """

    TOKEN_LAST_ACTIVITY = "last_activity_on"

    def get_data(self, date: Optional[datetime.datetime.date] = None) -> pd.DataFrame:
        """Determines active users for different timeframes

        Parameters
        ----------
        date : None as default --> getting data for yesterday

        Returns
        -------
        pd.DataFrame : dictionary showing number of users per email domain
            datetime,hzdr.de,desy.de,...
            This table head is an example and depends on the retrieved email domains.
        """

        if date is None:
            print("[INFO] No date specified. Getting data for yesterday")
            date = datetime.date.today() - datetime.timedelta(days=1)

        end_time = int(
            datetime.datetime.combine(date, datetime.datetime.min.time()).timestamp()
        )
        # Get number of active users from the overleaf-statistics API.
        current_datetime = datetime.datetime.now(datetime.timezone.utc)

        email_domains_dict = self.call_api("email_domains_of_active_users")

        email_domains = pd.Series(
            email_domains_dict.values(), index=email_domains_dict.keys()
        )

        datetime_column = pd.Series(
            [current_datetime.isoformat()],
            index=["datetime"],
        )

        return pd.concat([datetime_column, email_domains]).to_frame().T
