import datetime
import sys
from typing import Optional

import pandas as pd

from .gitlab_api import GitlabApiMetric


class GitlabProjectStats(GitlabApiMetric):
    """Getting statistics around projects

    Each project contains various attributes, served as a nested dict,
    from which we are interested in the following entries:

        'last_activity_at': '2022-05-11T11:05:09.447+02:00',

        'statistics': {'commit_count': 1,

        'storage_size': 203612487,

        'repository_size': 203612487,

        'wiki_size': 0,

        'lfs_objects_size': 0,

        'job_artifacts_size': 0,


    After obtaining the attributes, we are interested about several time frames.
    In order to obtain these information, we perform some calculations on the
    relevant data with pandas.


    Examples
    --------
    Example content of pd.DataFrame returned (splitted for length):
        datetime,number_of_projects,commit_count,storage_size,repository_size,wiki_size,l
        fs_objects_size,job_artifacts_size,
        last_active_1_days,last_active_7_days,last_active_14_days,
        last_active_30_days,last_active_60_days
        2022-05-11T10:06:09.496445+00:00,4001,685057,726297478129,
        232375072321,1830037160,156204680554,214183640072,69,288,417,627,995

    """

    def init(self):
        # Currently set static as in original codebase, will be refactored
        self.activity_period = [1, 7, 14, 30, 60]

    def get_data(self) -> pd.DataFrame:
        """Get data on projects; getting data for yesterday

        Returns
        -------
        pd.DataFrame
            fs_objects_size,job_artifacts_size,
            last_active_1_days,last_active_7_days,last_active_14_days,
            last_active_30_days,last_active_60_days
        """

        # Get number of active projecs from the GitLab API.
        activity_period_deltas = [
            datetime.timedelta(days=n) for n in self.activity_period
        ]
        reference_date = datetime.datetime.today().date()
        current_datetime = datetime.datetime.now(datetime.timezone.utc)

        # Creates list containing 0 which is len(self.activity_period)
        active_projects_since = [0] * len(self.activity_period)

        gl = self.gl_api()

        # list(iterator=True) returns a generator object.
        # This is Gitlabs API recommended way to iterate through a large number of items.
        projects = gl.projects.list(iterator=True, statistics="yes", per_page=250)

        total_projects = projects.total

        project_stats = pd.DataFrame()
        last_active_on = []

        for project in projects:
            project_attributes = pd.DataFrame([project.attributes["statistics"]])
            project_stats = pd.concat(
                [project_stats, project_attributes], ignore_index=True
            )
            last_active_on.append(project.attributes["last_activity_at"])

        # Check if this is the right axis, this contains alreauy all necessary values
        # Executing sum() on a Dataframe returns a Series, thus project_stats changes here
        project_stats = project_stats[
            [
                "commit_count",
                "storage_size",
                "repository_size",
                "wiki_size",
                "lfs_objects_size",
                "job_artifacts_size",
            ]
        ]
        project_stats = project_stats.sum(axis=0)
        project_stats = project_stats.astype("int64")

        last_active_on = pd.DataFrame(last_active_on)
        last_active_on = last_active_on.dropna()
        activity_periods = self.convert_dates(last_active_on)

        activity_periods[0] = reference_date - activity_periods[0]
        for num, delta in zip(self.activity_period, activity_period_deltas):
            activity_periods[f"last_active_{num}_days"] = activity_periods[0] < delta

        activity_periods = activity_periods.drop(columns=0)
        # Retuns a Series, not a DataFrame
        last_active = activity_periods.sum(axis=0)

        prepend = pd.Series(
            [current_datetime.isoformat(), total_projects],
            index=["datetime", "number_of_projects"],
        )

        result = pd.concat([prepend, project_stats, last_active]).to_frame().T

        return result.rename(
            columns={
                "commit_count": "total_commit_count",
                "storage_size": "total_storage_size",
                "repository_size": "total_repository_size",
                "wiki_size": "total_wiki_size",
                "lfs_objects_size": "total_lfs_object_size",
                "job_artifacts_size": "total_job_artifacts_size",
            }
        )
