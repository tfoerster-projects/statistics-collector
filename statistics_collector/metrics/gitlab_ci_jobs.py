import datetime
import json
import sys
from traceback import print_tb
from typing import Optional

import pandas as pd
import requests

from .metric import Metric, yesterday


class GitlabCIJobs(Metric):
    """Get data about CI jobs

    Getting the job count for each project.

    Needs two queries as one can only access the proper CI-Job information per project.
    Haven't found a better way yet.

    Examples
    --------
    Example content of pd.DataFrame returned:
        name,fullPath,JobCount
        experiment-backend-server,maxmustermann123,/experiment-backend-server,0
        paper_multiweek_radiomics,normajean123/paper_multiweek_radiomics,8
    """

    def init(self):
        self.uri = f"https://{self.settings['HOST_GITLAB']}/api/graphql"
        self.headers = {"Authorization": "Bearer " + self.settings["TOKEN_GITLAB"]}
        # TODO ! Magic strings -> Token !

    @staticmethod
    def _build_query(after: Optional[str] = None) -> str:
        """Builds custom graphql query

        Parameters
        ----------
        after : denotes endCursor, needed for navigating through multiple pages

        Returns
        -------
        Valid GraphQL query as String
        """
        projects_argument = f'(after: "{after}")' if after is not None else ""

        # Note: Escape the curly braces in f-strings by duplicating them
        return f"""
        {{
            projects{projects_argument} {{
                pageInfo {{
                    endCursor
                    startCursor
                    hasNextPage
            }}
            nodes {{
                name
                fullPath
                }}
            }}
        }}
        """

    @staticmethod
    def _build_ci_job_query(full_path: str) -> str:
        """Builds custom graphql query

        Parameters
        ----------
        full_path : path to project

        Returns
        -------
        Valid GraphQL query as String
        """

        return f"""
        {{
            project(fullPath: \"{full_path}\") {{
                name
                jobs {{
                    count
                }}
            }}
        }}
        """

    def _run_query(self, query) -> json:
        requested_data = requests.post(
            self.uri, json={"query": query}, headers=self.headers  # TODO Magic string
        )
        requested_data.raise_for_status()
        return requested_data.json()

    def get_data(self) -> pd.DataFrame:
        """Get CI jobs for each project; getting data for yesterday

        Returns
        -------
        pd.DataFrame
            name,fullPath,JobCount,date
        """
        # TODO option for conversion of user input to datetime.datetime.date
        # needs to be added when either CLI or config is implemented

        # Sanity checks for formatting
        # Print(f"recorded_after {recorded_after}")
        # Print(f"recorded_before {recorded_before}")

        init_query = self._build_query()
        q_response = self._run_query(init_query)

        # TODO find a more accessible way to deal with the complexity of
        # GraphQL queries.

        hasNextPage = q_response["data"]["projects"]["pageInfo"]["hasNextPage"]
        endCursor = q_response["data"]["projects"]["pageInfo"]["endCursor"]

        content = pd.DataFrame()
        # Pick relevant fields from response and remove pandas index
        filtered_response = pd.DataFrame(q_response["data"]["projects"]["nodes"])
        content = pd.concat([content, filtered_response], ignore_index=True)
        # Get all projects and paths
        while hasNextPage:
            current_query = self._build_query(endCursor)
            q_response = self._run_query(current_query)
            hasNextPage = q_response["data"]["projects"]["pageInfo"]["hasNextPage"]
            endCursor = q_response["data"]["projects"]["pageInfo"]["endCursor"]
            filtered_response = pd.DataFrame(q_response["data"]["projects"]["nodes"])
            content = pd.concat([content, filtered_response], ignore_index=True)

        # Slicing - only here for testing in ipython bc the queries take _long
        # content = content[:10]  # TODO: Only for test, remove me before merge

        # Get CI-Jobs per project
        jobs_per_project = pd.DataFrame()
        # Slicing - left here for debugging purposes
        # for fullPath in content["fullPath"][:10]:
        for fullPath in content["fullPath"]:
            current_query = self._build_ci_job_query(fullPath)
            q_response = self._run_query(current_query)
            job_assignment = {}
            job_assignment["fullPath"] = fullPath
            if q_response["data"]["project"] is None:
                job_assignment["JobCount"] = 0
            elif q_response["data"]["project"]["jobs"] is None:
                job_assignment["JobCount"] = 0
            else:
                job_assignment["JobCount"] = q_response["data"]["project"]["jobs"][
                    "count"
                ]

            # put-dict-into-a-dataframe
            job_assignment = pd.DataFrame([job_assignment])
            jobs_per_project = pd.concat(
                [jobs_per_project, job_assignment], ignore_index=True
            )

        # Concatenate both tables
        result = content.join(jobs_per_project.set_index("fullPath"), on="fullPath")
        result["JobCount"] = pd.to_numeric(result["JobCount"], downcast="integer")

        result["date"] = yesterday().isoformat()

        return result
