import datetime
from abc import ABC, abstractmethod
from typing import Optional

import pandas as pd


def yesterday() -> datetime.datetime.date:
    return datetime.date.today() - datetime.timedelta(days=1)


class Metric(ABC):
    """Abstract class providing mandatory logic.

    Each inheriting class has to implement the get_data() method.

    The method check_accepts_date() indicates whether the API supports getting historical data.
    This option is useful when data from a specific day in the past is needed.

    Unless a specific date is set, the default behavior is to fetch data
    from yesterday - today's data might not be complete yet at the time of execution.

    Attributes
    ----------
    settings : dictionary with parameters used in derived classes, e. g. access credentials
    """

    def __init__(self, settings: dict) -> None:
        self.settings = settings
        self._accepts_date = self.check_accepts_date()
        self.init()

    def init(self) -> None:
        pass

    @staticmethod
    def convert_dates(df: pd.DataFrame) -> pd.DataFrame:
        """Convert date formats.

        Convert isoformat string to datetime.datetime object ->
        convert to datetime.datetime object to datetime.date ->
        for further comparison with reference_date, which is from type datetime.date()

        Parameters
        ----------
        df : pd.DataFrame
            date to be converted

        Returns
        -------
        pd.DataFrame
            converted date
        """
        df = df.map(datetime.datetime.fromisoformat)
        df = df.map(lambda dt: dt.date())
        return df

    def check_accepts_date(self):
        argc = self.get_data.__code__.co_argcount
        arg_names = self.get_data.__code__.co_varnames[:argc]
        return "date" in arg_names

    def handle_date(
        self, date: Optional[datetime.datetime.date] = None
    ) -> datetime.datetime.date:
        if not self.accepts_date:
            assert (
                date is None
            ), f"{self.__class__.__name__} doesn't support accessing data at an arbitrary date."
            return None
        if date is None:
            print("[INFO] No date has been specified, getting data for yesterday.")
            return yesterday()
        return date

    def get_data_wrapper(self, date: Optional[datetime.date] = None) -> pd.DataFrame:
        if self._accepts_date:
            return self.get_data(date=self.handle_date(date))
        return self.get_data()

    @property
    def accepts_date(self) -> bool:
        return self._accepts_date

    @abstractmethod
    def get_data(self, date: Optional[datetime.date] = None) -> pd.DataFrame:
        pass
