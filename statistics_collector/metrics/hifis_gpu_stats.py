import datetime
import sys
from typing import Optional

import pandas as pd
import requests

from .metric import Metric


class HifisGpuStats(Metric):
    """Gets some basic statistics

    The statistics log is downloaded from HIFIS-GPU.

    Examples
    --------
    Example content of pandas DataFrame returned (splitted for length):
        datetime,storage_total,users_total,users_active_12h
        2022-05-11 10:03:40,45678789,15,2
    """

    def call_api(self, year: int, month: int):
        """
        Authenticated HTTPS GET download of one statistics log file from the HIFIS-GPU instance

        Parameters
        ----------
        month : 1 .. 12
            month of the statistics log file to download; Each log file contains data of one month.
        year : int
            select statistics log file to download by year

        Returns
        -------
        str
            HTTP Body containing the raw content of the statistics log file

        Example
        -------
        Returned statistics file content:
            1698098659 70721689 17 1\n
            1698141840 78024799 17 3\n
            1698185063 89838014 14 1
        """

        http_api_response = requests.get(
            f"https://{self.settings['HOST_HIFIS_GPU']}/statistics/statistics_{year}_{month}.log",
            headers={"Authorization": f"Bearer {self.settings['TOKEN_HIFIS_GPU']}"},
        )
        http_api_response.raise_for_status()
        return http_api_response.text

    def get_data(self, date: Optional[datetime.datetime.date] = None) -> pd.DataFrame:
        """Determines some statistics values recorded at a given time.

        Parameters
        ----------
        date : None as default --> getting data for yesterday
            This parameter selects the latest statistics log entry not new than the given date.

        Returns
        -------
        pd.DataFrame
            datetime,storage_total,users_total,users_active_12h
        """

        if date is None:
            print("[INFO] No date specified. Getting data for yesterday")
            date = datetime.date.today() - datetime.timedelta(days=1)

        ts = datetime.datetime.combine(date, datetime.datetime.max.time()).timestamp()

        raw_stats = self.call_api(date.year, date.month)

        stats_data = [e.split(" ") for e in raw_stats.split("\n") if len(e) > 0]

        stats_older = [e for e in stats_data if float(e[0]) < ts]

        stats_older.sort(key=lambda e: e[0])

        selected_data = stats_older[-1]

        stats = pd.Series(
            [datetime.datetime.fromtimestamp(float(selected_data[0])).isoformat()]
            + selected_data[1:],
            index=["datetime", "storage_total", "users_total", "users_active_12h"],
        )

        return stats.to_frame().T
