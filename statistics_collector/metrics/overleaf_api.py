import requests

from .metric import Metric


class OverleafApiMetric(Metric):
    def call_api(self, endpoint, params={}):
        http_api_response = requests.get(
            f"https://{self.settings['HOST_OVERLEAF']}/statistics/{endpoint}",
            params=params,
            headers={"Authorization": f"Bearer {self.settings['TOKEN_OVERLEAF']}"},
        )
        http_api_response.raise_for_status()
        return http_api_response.json()
