import datetime
import sys
from typing import Optional

import pandas as pd

from .gitlab_api import GitlabApiMetric


class GitlabActiveUsers(GitlabApiMetric):
    """Gets amount of active users

    Each user contains various attributes, served as a dict,
    from which we are interested in the following entry:

    'last_activity_on': '2022-05-11'

    After obtaining the last_activity date for each user and the number of total users,
    we are interested about several time frames.
    In order to obtain these information, we perform some calculations on the
    relevant data with pandas.

    Examples
    --------
    Example content of pandas DataFrame returned (splitted for length):
        datetime,number_of_users,last_active_1_days,last_active_7_days,
        last_active_14_days,last_active_30_days,last_active_60_days
        2022-05-11T10:03:40.432770+00:00,7447,416,1431,2093,2909,3619
    """

    TOKEN_LAST_ACTIVITY = "last_activity_on"

    def init(self):
        # Currently set static as in original codebase, will be refactored
        self.activity_period = [1, 7, 14, 30, 60]

    def get_data(self) -> pd.DataFrame:
        """Determines active users for different timeframes; getting data for yesterday

        Returns
        -------
        pd.DataFrame
            datetime,number_of_users,last_active_1_days,last_active_7_days,
            last_active_14_days,last_active_30_days,last_active_60_days
        """

        # Get number of active users from the GitLab API.
        activity_period_deltas = [
            datetime.timedelta(days=n) for n in self.activity_period
        ]
        # reference_date output: datetime.date(2022, 5, 16)
        reference_date = datetime.datetime.today().date()
        current_datetime = datetime.datetime.now(datetime.timezone.utc)

        gl = self.gl_api()

        # list(iterator=True) returns a generator object.
        # This is Gitlabs API recommended way to iterate through a large
        # number of items.
        users = gl.users.list(iterator=True, per_page=250)

        # Get most recent date of activity from each user
        last_active_on = [
            user.attributes[self.TOKEN_LAST_ACTIVITY] for user in list(users)
        ]

        last_active_on = pd.DataFrame(last_active_on)

        total_users = last_active_on.size

        last_active_on = last_active_on.dropna()
        activity_periods = self.convert_dates(last_active_on)

        activity_periods[0] = reference_date - activity_periods[0]
        for n, delta in zip(self.activity_period, activity_period_deltas):
            activity_periods[f"last_active_{n}_days"] = activity_periods[0] < delta
            # TODO find a better way to deal with this magic string

        activity_periods = activity_periods.drop(columns=0)
        # Retuns a Series, not a DataFrame
        last_active = activity_periods.sum(axis=0)

        prepend = pd.Series(
            [current_datetime.isoformat(), total_users],
            index=["datetime", "number_of_users"],
        )

        result = pd.concat([prepend, last_active]).to_frame().T

        return result
