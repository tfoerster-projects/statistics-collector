import datetime
import sys
from typing import Optional

import pandas as pd
import requests

from .metric import Metric


class MattermostStats(Metric):
    """Retrieving general mattermost statistics

    Retrieving and transforming statistical data from mattermost.

    Examples
    --------
    Example content of pd.DataFrame returned:
        datetime,post_count,team_count,daily_active_users,monthly_active_users
        2022-08-02T13:09:16.219847+00:00,4533697,1110,1650,2975

    """

    def init(self):
        # Mattermost API doesen't offer an date option
        self.host = self.settings["HOST_MATTERMOST"]
        self.token = self.settings["TOKEN_MATTERMOST"]

    def get_data(self) -> pd.DataFrame:
        """Get analytics data from the Mattermost API. getting data for yesterday

        Returns
        -------
        pd.DataFrame :
            datetime,post_count,team_count,daily_active_users,monthly_active_users
        """

        current_datetime = datetime.datetime.now(datetime.timezone.utc)

        request_url = f"https://{self.host}/api/v4/analytics/old"
        request_token = f"Bearer {self.token}"
        request_headers = {"Authorization": request_token}

        requested_data = requests.get(url=request_url, headers=request_headers)
        requested_data.raise_for_status()

        raw_data = requested_data.json()

        project_stats = pd.DataFrame(raw_data)
        project_stats = project_stats.pivot_table(values="value", columns="name")
        project_stats = project_stats.rename_axis(None, axis=1)

        concat = pd.DataFrame(
            [current_datetime.isoformat()], index=["datetime"], columns=["value"]
        )

        result = pd.concat([concat, project_stats.T]).T
        result = result[
            [
                "datetime",
                "post_count",
                "team_count",
                "daily_active_users",
                "monthly_active_users",
            ]
        ]

        return result
