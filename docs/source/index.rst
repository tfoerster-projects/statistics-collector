.. statistics-collector documentation master file, created by
   sphinx-quickstart on Tue Aug  2 14:07:36 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to statistics-collector's documentation!
================================================
lorem ipsum

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   statistics_collector.metrics
   statistics_collector
   modules
   tests

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
