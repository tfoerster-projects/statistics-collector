tests package
=============

Submodules
----------

tests.test\_queries module
--------------------------

.. automodule:: tests.test_queries
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: tests
   :members:
   :undoc-members:
   :show-inheritance:
