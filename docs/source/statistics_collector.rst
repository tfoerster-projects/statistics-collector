statistics\_collector package
=============================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   statistics_collector.metrics

Submodules
----------

statistics\_collector.generate\_statistics module
-------------------------------------------------

.. automodule:: statistics_collector.generate_statistics
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: statistics_collector
   :members:
   :undoc-members:
   :show-inheritance:
