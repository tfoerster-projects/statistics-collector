import datetime
from typing import Optional

import pandas

from .metric import Metric


class ExampleMetricStats(Metric):
    def __init__(self, host: str, token: str) -> None:
        super().__init__(host, token, accepts_date=False)
        self.host = host
        self.token = token

    # Implement further helper functions if needed

    def get_data(self, date: Optional[datetime.datetime.date] = None) -> pd.DataFrame:
        # Implement your custom data retrieval and processing here
        # No need to save your results, only return a pandas dataframe, runner.py will handle the rest for you.
        pass
