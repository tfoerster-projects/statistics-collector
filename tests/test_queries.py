import pytest

from statistics_collector.metrics.gitlab_ci_jobs import GitlabCIJobs

# Symbolic constants to fall back on during the tests
# to ensure we do not have to debug typos all day long

DUMMY_HOST = "dummy_host"
DUMMY_TOKEN = "dummy_token"
CORRECT_EMPTY_PROJECT_QUERY = """
    {
        projects {
            pageInfo {
                endCursor
                startCursor
                hasNextPage
        }
        nodes {
            name
            fullPath
            }
        }
    }
"""

PROJECT_QUERY_PARAMETER = "dummy_parameter"
CORRECT_PARAMETRIZED_PROJECT_QUERY = """
    {
        projects(after: \"dummy_parameter\") {
            pageInfo {
                endCursor
                startCursor
                hasNextPage
        }
        nodes {
            name
            fullPath
            }
        }
    }
"""


@pytest.fixture
def generate_gitlab_ci_jobs_dummy():
    """
    Set up a dummy instance for a GitlabCIJobs metric.
    ATTENTION! This can only be used in tests that do not require an actual
    connection to the gitlab servers!

    Returns:
        A GitlabCIJobs instance with dummy parameters for host and token
    """
    return GitlabCIJobs(
        settings={"HOST_GITLAB": DUMMY_HOST, "TOKEN_GITLAB": DUMMY_TOKEN}
    )


def test_project_query_without_parameters(generate_gitlab_ci_jobs_dummy):
    """
    Test a project query without parameter.

    For comparison, whitespace is ignored.

    Parameters:
        generate_gitlab_ci_jobs_dummy:
            The fixture used to generate the UUT
    """

    # UUT = Unit under Test
    uut = generate_gitlab_ci_jobs_dummy

    query = uut._build_query()
    assert query.split() == CORRECT_EMPTY_PROJECT_QUERY.split()


def test_project_query_with_parameter(generate_gitlab_ci_jobs_dummy):
    """
    Test a project query with parameter.

    For comparison, whitespace is ignored.

    Parameters:
        generate_gitlab_ci_jobs_dummy:
            The fixture used to generate the UUT
    """
    uut = generate_gitlab_ci_jobs_dummy

    query = uut._build_query(after=PROJECT_QUERY_PARAMETER)
    assert query.split() == CORRECT_PARAMETRIZED_PROJECT_QUERY.split()
